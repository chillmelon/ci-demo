require("dotenv").config();
const port = process.env.PORT || 80;
const fs = require("fs");
const path = require("path");
const express = require("express");
const multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });
const app = express();
const imgur = require("./library/imgur");
const db = require("./library/dbconnect");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.get("/", (req, res) => {
	res.render("index", {
		msg: "This is index",
	});
});

app.get("/meme", (req, res) => {
	let tag = req.query.tag;
	let promise = db.search(tag);
	promise.then(result => {
		if (!result[0]) {
			res.render("index", {
				msg: "Not found",
			});
			return;
		}
		res.render("index", {
			msg: "Ok",
			pics: result[0].url,
		});
	});
});

app.post("/upload", upload.single("img"), (req, res) => {
	let img = req.file.buffer.toString("base64");
	let tag = req.body.tag;
	imgur(img).then(link => {
		let promise = db.add(tag, link);
		promise.then(() => {
			res.render("index", {
				msg: "uploaded",
				pics: link,
			});
			}
		);
	});
});


app.listen(port, () => console.log(`App is running on port ${port}`));
