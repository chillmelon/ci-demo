var mysql = require('mysql');

var con = mysql.createConnection({
  host: process.env.DB_HOST || '127.0.0.1',
	port: 3306,
  user: "meme",
  password: "6666",
	database: "meme"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

function add(tag, url) {
	let value = [tag, url];
	let sql = "INSERT INTO `meme` (tag, url) VALUES (?, ?)";
	let p = new Promise((resolve, reject) => {
		con.query(sql, value, (err, result) => {
			if (err) throw err;
			resolve(result);
		});
	});
	return p;
};

function search(tag) {
	let sql = "SELECT * FROM `meme` WHERE tag = ? ORDER BY RAND() LIMIT 1";
	let value = tag;
	let p = new Promise((resolve, reject) => {
		con.query(sql, value, (err, result) => {
			if (err) throw err;
			resolve(result);
		});
	});
	return p;
}

module.exports = {
	add,
	search,
}
