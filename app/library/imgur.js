const rp = require("request-promise");
module.exports = async function (img) {
	let options = {
		'method': 'POST',
		'url': 'https://api.imgur.com/3/image',
		'headers': {
			'Authorization': 'Client-ID 858eadbb3d74199' },
		formData: {
			'image': img,
		}
	};

	let result = await rp(options)
		.then((body) => {
			console.log("POST succeeded");
			let link = JSON.parse(body).data.link;
			return link;
		})
		.catch((err) => {
			console.log("error", err);
		})
	return result;
};
